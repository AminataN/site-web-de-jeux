$(document).ready(function() {
    let box_id;

    // on cache les box
    // $('.box').hide();
    $('.box').toggle();

    // on affiche la premiere div
    // $('.box').show();
    $('#box_1').toggle('slow');

    // au clique sur le bouton suivant
    $('#container button').on('mousedown', function(){
        // on recupere le numero de box
        box_id = $(this).data('id');
        // Cacher la box actuellement afficher
        $('#box_'+box_id).toggle('slow');
        // increment id
        box_id++;
        // on affiche la box suivant
        $('#box_'+box_id).toggle('slow');
    });
});